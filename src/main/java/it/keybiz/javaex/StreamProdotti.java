package it.keybiz.javaex;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamProdotti {

    // mi appoggio ad un factory method che genera a rotazione
    // la stessa lista di libri
    public List<Prodotti> generaListaProdotti(int n) {
        return Stream.generate(Magazziniere::gen)
            .limit(n)
            .collect(Collectors.toList());
    }

    public long contaProdottiDiBellezza(List<Prodotti> list) {
        return 0;
    }

    public List<Prodotti> prezzoCompresoTra12e15(List<Prodotti> list) {
        return null;
    }

    public List<String> filtraListaProdottiAlimentariEBellezza(List<Prodotti> list) {
        return null;
    }

    public List<Prodotti> generaListaProdottiAlimentari(int n) {
        return null;
    }

    public boolean checkSePresenteBurningChrome(List<Prodotti> list) {
        return true;
    }

    public int sommaCosti_reduce(List<Prodotti> list) {
        return 0;
    }

    public int sommaCosti_sum(List<Prodotti> list) {
        return 0;
    }

    public double sommaCostiInDollari(double EUR_USD, List<Prodotti> list) {
        return 0.0;
    }

    public Optional<Prodotti> prodottoMenoCaroDa12InSu(List<Prodotti> list) {
        return null;
    }

    public List<Prodotti> prodottoOrdinatiPerPrezzo(List<Prodotti> list) {
        return null;
    }

    // Titolo: "Pozione Harry Potter 1" "Pozione Harry Potter 2"... "Pozione Harry Potter n"
    // categoria: ALIMENTARI, prezzo: 15 euro
    public List<Prodotti> generaPozioneHarryPotterDa15Euro(int n) {
        return null;
    }

    public List<Prodotti> mescolaLista(List<Prodotti> list) {
        return null;
    }

    public Optional<Prodotti> primoPiuCaroDelPrecedente(List<Prodotti> list) {
        return null;
    }

}
