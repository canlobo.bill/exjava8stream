package it.keybiz.javaex;

import it.keybiz.javaex.Prodotti.Categoria;

public class Magazziniere {

	private Magazziniere() {}

	private static int i = 0;
	
	private static Prodotti[] prodPool = new Prodotti[] {
			new Prodotti("Biscotti", Categoria.ALIMENTARI, 40),
			new Prodotti("Pane", Categoria.ALIMENTARI, 25),

			new Prodotti("Profumo", Categoria.BELLEZZA, 15),
			new Prodotti("Cipria", Categoria.BELLEZZA, 12),
			new Prodotti("Rossetto", Categoria.BELLEZZA, 10),

			new Prodotti("Stura tutto", Categoria.CASALINGHI, 8),
			new Prodotti("Carta Igienica", Categoria.CASALINGHI, 12),

			new Prodotti("Candele", Categoria.OFFICINA, 5),
			new Prodotti("CD", Categoria.INFORMATICA, 35) };

	public static Prodotti gen() {
		return prodPool[i++ % prodPool.length];
	}

	public static void reset() {
		i = 0;
	}
}